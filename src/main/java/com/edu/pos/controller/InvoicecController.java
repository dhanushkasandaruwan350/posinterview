package com.edu.pos.controller;

import com.edu.pos.payload.CustomerRequest;
import com.edu.pos.payload.InvoiceRequest;
import com.edu.pos.payload.OrderDetails;
import com.edu.pos.service.InvoiceService;
import com.edu.pos.service.impl.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/invoice")
public class InvoicecController {

    @Autowired
    InvoiceService invoiceService;
    @Autowired
    FileService fileService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> newInvoice(@RequestBody InvoiceRequest request) throws Exception {
        return ResponseEntity.ok(invoiceService.createInvoice(request));
    }

    @GetMapping("{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> downloadFile(@PathVariable String filename) throws IOException {
        System.out.println("hit");
        Resource file = fileService.download(filename);
        Path path = file.getFile()
                .toPath();

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, Files.probeContentType(path))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }
}
