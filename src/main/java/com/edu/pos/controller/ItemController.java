package com.edu.pos.controller;

import com.edu.pos.payload.CustomerRequest;
import com.edu.pos.payload.ItemRequest;
import com.edu.pos.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/item")
public class ItemController {

    @Autowired
    ItemService itemService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> newItem(@RequestBody ItemRequest request) throws Exception {
        return ResponseEntity.ok(itemService.saveItem(request));
    }

    @RequestMapping(value = "/check", method = RequestMethod.POST)
    public ResponseEntity<?> checkItem(@RequestBody String id) {
        return ResponseEntity.ok(itemService.checkItem(id));
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<?> listItem() {
        return ResponseEntity.ok(itemService.allItems());
    }
}
