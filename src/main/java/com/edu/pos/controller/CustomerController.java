package com.edu.pos.controller;

import com.edu.pos.payload.CustomerRequest;
import com.edu.pos.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> newCustomer(@RequestBody CustomerRequest request) {
        return ResponseEntity.ok(customerService.saveNewCustomer(request));
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<?> getCustomers() {
        return ResponseEntity.ok(customerService.customerList());
    }

    @RequestMapping(value = "/check", method = RequestMethod.POST)
    public ResponseEntity<?> checkCustomer(@RequestBody String contact) {
        return ResponseEntity.ok(customerService.checkByContact(contact));
    }
}
