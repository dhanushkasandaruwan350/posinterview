package com.edu.pos.util;

import com.edu.pos.payload.InvoiceRequest;
import com.edu.pos.payload.OrderDetails;
import com.edu.pos.repository.ItemRepository;
import com.spire.doc.Document;
import com.spire.doc.FileFormat;
import com.spire.doc.Table;
import com.spire.doc.fields.Field;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class GenerateInvoice {

    @Autowired
    ItemRepository repository;

    public String generate(InvoiceRequest request) {
        Document doc = new Document();
        doc.loadFromFile("/opt/tomcat/webapps/invoice.docx");
        doc.replace("#InvoiceNum", "17854", true, true);
        doc.replace("#CompanyName", "ABC Company", true, true);
        doc.replace("#CompanyAddress", "122 4th Ave", true, true);
        doc.replace("#CityStateZip", "New York, NY 10011", true, true);
        doc.replace("#Country", "United States", true, true);
        doc.replace("#Tel1", "111-222-333", true, true);
        doc.replace("#ContactPerson", request.getCustomer_name(), true, true);
        doc.replace("#ShippingAddress", "122 4th Ave", true, true);
        doc.replace("#Tel2", request.getCustomer_contact(), true, true);

        String[][] purchaseData = {};
        for (OrderDetails details : request.getList()) {
            String[] newValuesArray = {repository.getById(details.getItem()).getName(), details.getQty()+"",details.getPrice()};
            purchaseData = Arrays.copyOf(purchaseData, purchaseData.length + 1);
            purchaseData[purchaseData.length - 1] = newValuesArray;
        }

        writeDataToDocument(doc, purchaseData);
        doc.isUpdateFields(true);
        doc.saveToFile("/opt/tomcat/webapps/Invoice.pdf", FileFormat.PDF);
        return "ok";
    }

    private static void writeDataToDocument(Document doc, String[][] purchaseData) {
        Table table = doc.getSections().get(0).getTables().get(2);
        if (purchaseData.length > 1) {
            addRows(table, purchaseData.length - 1);
        }
        fillTableWithData(table, purchaseData);
    }

    private static void fillTableWithData(Table table, String[][] data) {
        for (int r = 0; r < data.length; r++) {
            for (int c = 0; c < data[r].length; c++) {
                //fill data in cells
                table.getRows().get(r + 1).getCells().get(c).getParagraphs().get(0).setText(data[r][c]);
            }
        }
    }

    private static void addRows(Table table, int rowNum) {
        for (int i = 0; i < rowNum; i++) {
            //insert specific number of rows by cloning the second row
            table.getRows().insert(2 + i, table.getRows().get(1).deepClone());
            //update formulas for Total
            for (Object object : table.getRows().get(2 + i).getCells().get(3).getParagraphs().get(0).getChildObjects()
            ) {
                if (object instanceof Field) {
                    Field field = (Field) object;
                    field.setCode(String.format("=B%d*C%d\\# \"0.00\"", 3 + i, 3 + i));
                }
                break;
            }
        }
        //update formula for Total Tax
        for (Object object : table.getRows().get(4 + rowNum).getCells().get(3).getParagraphs().get(0).getChildObjects()
        ) {
            if (object instanceof Field) {
                Field field = (Field) object;
                field.setCode(String.format("=D%d*0.05\\# \"0.00\"", 3 + rowNum));
            }
            break;
        }
        //update formula for Balance Due
        for (Object object : table.getRows().get(5 + rowNum).getCells().get(3).getParagraphs().get(0).getChildObjects()
        ) {
            if (object instanceof Field) {
                Field field = (Field) object;
                field.setCode(String.format("=D%d+D%d\\# \"$#,##0.00\"", 3 + rowNum, 5 + rowNum));
            }
            break;
        }
    }
}
