package com.edu.pos.repository;

import com.edu.pos.modal.Customer;
import com.edu.pos.modal.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepositoty extends JpaRepository<Order,Long> {
    long countAllByCustomerEquals(Customer customer);
}
