package com.edu.pos.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response {
    String text;
    String code;
    boolean status;

    public Response() {
    }

    public Response(String text, String code, boolean status) {
        this.text = text;
        this.code = code;
        this.status = status;
    }
}
