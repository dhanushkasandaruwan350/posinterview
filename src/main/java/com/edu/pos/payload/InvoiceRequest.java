package com.edu.pos.payload;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class InvoiceRequest {
    String customer_contact;
    String customer_name;
    String pay_by;
    int item_count;
    String total;
    List<OrderDetails> list;
}
