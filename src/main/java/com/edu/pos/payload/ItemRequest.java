package com.edu.pos.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemRequest {
    private long id;
    private String name;
    private String qty;
    private String cost;
    private String price;
    private String expire;
}
