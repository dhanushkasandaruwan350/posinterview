package com.edu.pos.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderDetails {
    long item;
    int qty;
    String price;
    double sub_total;
}
