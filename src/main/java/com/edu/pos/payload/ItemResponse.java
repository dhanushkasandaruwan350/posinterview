package com.edu.pos.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemResponse {
    private long id;
    private String name;
    private String qty;
    private String cost;
    private String price;
    private String expire;
    private String reg;
    private String upd;
    private int orders;
    private int sold;

    public ItemResponse() {
    }

    public ItemResponse(long id, String name, String qty, String cost, String price, String expire,String reg,String upd, int orders, int sold) {
        this.id = id;
        this.name = name;
        this.qty = qty;
        this.cost = cost;
        this.price = price;
        this.expire = expire;
        this.reg = reg;
        this.upd = upd;
        this.orders = orders;
        this.sold = sold;
    }
}
