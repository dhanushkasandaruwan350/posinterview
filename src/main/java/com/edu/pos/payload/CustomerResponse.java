package com.edu.pos.payload;

import com.edu.pos.modal.Customer;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerResponse {
    private long id;
    private String name;
    private String contact;
    private String regdate;
    private long invoices;

    public CustomerResponse() {
    }

    public CustomerResponse(long id, String name, String contact, String regdate, long invoices) {
        this.id = id;
        this.name = name;
        this.contact = contact;
        this.regdate = regdate;
        this.invoices = invoices;
    }

    public CustomerResponse(Customer customer, int invoices) {
        this.id = customer.getId();
        this.name = customer.getName();
        this.contact = customer.getContact();
        this.regdate = customer.getCreatedAt().toString().split("\\.")[0];
        this.invoices = invoices;
    }
}
