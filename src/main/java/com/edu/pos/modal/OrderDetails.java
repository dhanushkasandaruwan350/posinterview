package com.edu.pos.modal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "T_ORDER_DETAILS")
@Getter
@Setter
public class OrderDetails extends AuditModal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORDER_DETAILS_ID")
    private long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ORDER_ID", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Order order;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ITEM", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Item item;

    @Column(name = "QUANTITY")
    private long qty;

    @Column(name = "PRICE")
    private String price;

    @Column(name = "SUB_TOTAL")
    private double sub_total;

    public OrderDetails() {
    }

    public OrderDetails(Order order, Item item, long qty, String price, double sub_total) {
        this.order = order;
        this.item = item;
        this.qty = qty;
        this.price = price;
        this.sub_total = sub_total;
    }
}
