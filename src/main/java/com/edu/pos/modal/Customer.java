package com.edu.pos.modal;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "T_CUSTOMER")
@Getter
@Setter
public class Customer extends AuditModal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CUSTOMER_ID")
    private long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CONTACT")
    private String contact;

    public Customer() {
    }

    public Customer(String name, String contact) {
        this.name = name;
        this.contact = contact;
    }
}
