package com.edu.pos.modal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "T_ORDER")
@Getter
@Setter
public class Order extends AuditModal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORDER_ID")
    private long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "CUSTOMER", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Customer customer;

    @Column(name = "ITEMS")
    private long items;

    @Column(name = "PAID_BY")
    private String paid;

    @Column(name = "TOTAL")
    private String total;

    public Order() {
    }

    public Order(Customer customer, long items, String paid, String total) {
        this.customer = customer;
        this.items = items;
        this.paid = paid;
        this.total = total;
    }
}
