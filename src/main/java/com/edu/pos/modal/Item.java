package com.edu.pos.modal;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "T_ITEM")
@Getter
@Setter
public class Item extends AuditModal{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ITEM_ID")
    private long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "QUANTITY")
    private String quantity;

    @Column(name = "PURCHASE_COST")
    private String purchase;

    @Column(name = "PRICE")
    private String price;

    @Column(name = "EXPIRE")
    private String expire;

    public Item() {
    }

    public Item(long id, String name, String quantity, String purchase, String price, String expire) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.purchase = purchase;
        this.price = price;
        this.expire = expire;
    }
}
