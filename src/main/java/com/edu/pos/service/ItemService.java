package com.edu.pos.service;

import com.edu.pos.payload.ItemRequest;
import com.edu.pos.payload.ItemResponse;
import com.edu.pos.payload.Response;

import java.util.List;

public interface ItemService {
    Response saveItem(ItemRequest request) throws Exception;
    ItemResponse checkItem(String id);
    List<ItemResponse> allItems();
}
