package com.edu.pos.service;

import com.edu.pos.modal.Customer;
import com.edu.pos.payload.CustomerRequest;
import com.edu.pos.payload.CustomerResponse;

import java.util.List;

public interface CustomerService {
    Customer saveNewCustomer(CustomerRequest request);
    List<CustomerResponse> customerList();
    CustomerResponse checkByContact(String contact);
}
