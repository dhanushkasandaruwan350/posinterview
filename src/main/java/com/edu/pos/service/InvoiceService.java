package com.edu.pos.service;

import com.edu.pos.payload.InvoiceRequest;
import com.edu.pos.payload.OrderDetails;
import com.edu.pos.payload.Response;

public interface InvoiceService {

    Response createInvoice(InvoiceRequest request) throws Exception;

}
