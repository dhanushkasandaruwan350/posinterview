package com.edu.pos.service.impl;

import com.edu.pos.modal.Customer;
import com.edu.pos.payload.CustomerRequest;
import com.edu.pos.payload.CustomerResponse;
import com.edu.pos.repository.CustomerRepository;
import com.edu.pos.repository.OrderRepositoty;
import com.edu.pos.service.CustomerService;
import com.edu.pos.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    OrderRepositoty orderRepositoty;

    @Override
    public Customer saveNewCustomer(CustomerRequest request) {
        Customer customer = customerRepository.findCustomerByContactEquals(request.getContact());
        if (customer == null) customer = new Customer(request.getName(), request.getContact());
        else customer.setName(request.getName());
        return customerRepository.save(customer);
    }

    @Override
    public List<CustomerResponse> customerList() {
        return customerRepository.findAll().stream().map(customer -> new CustomerResponse(customer.getId(), customer.getName(), customer.getContact(), customer.getCreatedAt().toString().split("\\.")[0], (int) orderRepositoty.countAllByCustomerEquals(customer))).collect(Collectors.toList());
    }

    @Override
    public CustomerResponse checkByContact(String contact) {
        System.out.println(contact);
        Customer customer = customerRepository.findCustomerByContactEquals(contact);
        return customer != null ? new CustomerResponse(customer, (int) orderRepositoty.countAllByCustomerEquals(customer)) : null;
    }
}
