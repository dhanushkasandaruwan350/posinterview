package com.edu.pos.service.impl;

import com.edu.pos.modal.Customer;
import com.edu.pos.modal.Item;
import com.edu.pos.modal.Order;
import com.edu.pos.payload.InvoiceRequest;
import com.edu.pos.payload.OrderDetails;
import com.edu.pos.payload.Response;
import com.edu.pos.repository.CustomerRepository;
import com.edu.pos.repository.ItemRepository;
import com.edu.pos.repository.OrderDetailsRepository;
import com.edu.pos.repository.OrderRepositoty;
import com.edu.pos.service.InvoiceService;
import com.edu.pos.util.GenerateInvoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    ItemRepository repository;
    @Autowired
    OrderRepositoty orderRepositoty;
    @Autowired
    OrderDetailsRepository orderDetailsRepository;
    @Autowired
    GenerateInvoice generateInvoice;

    @Override
    @Transactional
    public Response createInvoice(InvoiceRequest request) throws Exception {
        Customer customer = getCustomer(request.getCustomer_contact(), request.getCustomer_name());
        Order order = orderRepositoty.save(new Order(customer, request.getItem_count(), request.getPay_by(), request.getTotal()));
        Item item;
        for (OrderDetails details : request.getList()) {
            item = repository.getById(details.getItem());
            orderDetailsRepository.save(new com.edu.pos.modal.OrderDetails(order, item, details.getQty(),
                    details.getPrice(), details.getSub_total()));
            item.setQuantity(Integer.parseInt(item.getQuantity()) - details.getQty() + "");
            repository.save(item);
        }
        generateInvoice.generate(request);
        return new Response("Order Created!", "OK", true);
    }

    private Customer getCustomer(String customer_contact, String customer_name) {
        Customer customer = customerRepository.findCustomerByContactEquals(customer_contact);
        return customer == null ? customerRepository.save(new Customer(customer_name, customer_contact)) : customer;
    }
}
