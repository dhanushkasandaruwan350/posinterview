package com.edu.pos.service.impl;

import com.edu.pos.modal.Item;
import com.edu.pos.payload.ItemRequest;
import com.edu.pos.payload.ItemResponse;
import com.edu.pos.payload.Response;
import com.edu.pos.repository.ItemRepository;
import com.edu.pos.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    ItemRepository repository;

    @Override
    public Response saveItem(ItemRequest request) throws Exception {
        repository.save(new Item(request.getId(), request.getName(), request.getQty(), request.getCost(), request.getPrice(),
                request.getExpire()));
        return new Response("Successfully Saved!", "OK", true);
    }

    @Override
    public ItemResponse checkItem(String id) {
        try {
            Item item = repository.getById(Long.valueOf(id));
            return new ItemResponse(item.getId(), item.getName(), item.getQuantity(), item.getPurchase(), item.getPrice(),
                    item.getExpire(), item.getCreatedAt().toString().split("\\.")[0], item.getUpdatedAt().toString().split("\\.")[0], 0, 0);
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    @Override
    public List<ItemResponse> allItems() {
        return repository.findAll().stream().map(item -> new ItemResponse(item.getId(), item.getName(), item.getQuantity(), item.getPurchase(), item.getPrice(),
                item.getExpire(), item.getCreatedAt().toString().split("\\.")[0], item.getUpdatedAt().toString().split("\\.")[0], 0, 0)).collect(Collectors.toList());
    }
}
