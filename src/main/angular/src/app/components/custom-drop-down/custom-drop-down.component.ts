import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-custom-drop-down',
  templateUrl: './custom-drop-down.component.html',
  styleUrls: ['./custom-drop-down.component.css']
})
export class CustomDropDownComponent implements OnInit {
  @Input()
  dataList: any[] = [];
  @Output()
  selected: EventEmitter<any> = new EventEmitter<any>();
  display: any = 'Please Select';

  constructor() {
  }

  ngOnInit(): void {

  }

}
