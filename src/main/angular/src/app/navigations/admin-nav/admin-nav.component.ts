import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-admin-nav',
  templateUrl: './admin-nav.component.html',
  styleUrls: ['./admin-nav.component.css']
})
export class AdminNavComponent implements OnInit {
  items = [
    {name: 'Dash', active: false},
    {name: 'Invoice', active: false},
    {name: 'Customer', active: false},
    {name: 'Stock', active: false}
  ];

  constructor() {
  }

  ngOnInit(): void {
  }

  toggleClass(item: any) {
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.items.length; i++) {
      this.items[i].active = false;
    }
    item.active = !item.active;
  }

}
