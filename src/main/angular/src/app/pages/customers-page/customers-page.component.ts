import {Component, OnInit} from '@angular/core';
import {CustomerServiceService} from "../../service/customer-service.service";
import {NotificationService} from "../../service/notification.service";

declare var $: any;

@Component({
  selector: 'app-customers-page',
  templateUrl: './customers-page.component.html',
  styleUrls: ['./customers-page.component.css']
})
export class CustomersPageComponent implements OnInit {
  name = '';
  contact = '';
  response = 2;
  res_title = '';
  res_msg = '';
  has_alert = false;

  customer_list: any = [];
  duplicate = {
    name: '',
    regdate: ''
  }
  filter = '';
  filter_list: any = [];

  constructor(private CustomerService: CustomerServiceService, protected _notificationSvc: NotificationService) {
  }

  ngOnInit(): void {

  }

  ngAfterViewInit() {
    this.has_alert = false;
    this.getCustomerList();
  }

  checkCustomer() {
    this.CustomerService.check_customer(this.contact).subscribe(res => {
      if (res === null) {
        this.createCustomer();
      } else {
        this.duplicate = res;
      }
    })
  }

  private getCustomerList() {
    this.CustomerService.customer_list().subscribe(res => {
      this.customer_list = res;
      this.filter_list = res;
    }, error => {
      this.showMessage('Failed to load customers!', 'Can not load customer list. \n Please refresh the page to try again.', 1);
    });
  }

  createCustomer() {
    this.CustomerService.create_customer(this.name, this.contact).subscribe(res => {
      this.showMessage('Customer Record Created!', res.name + ' will be available in billing page shortly.', 0);
      this.clear();
      this.getCustomerList();
    }, error => {
      this.showMessage('Unexpected Error!', 'Sorry for the trouble!\nPlease contact your support center.', 1);
    });
  }

  hideError() {
    $('.ui .message').transition('slide down');
    this.has_alert = false;
  }

  // @ts-ignore
  showMessage(title, message, type) {
    this.response = type;
    this.res_title = title;
    this.res_msg = message;
    this.has_alert = true;
    $('.ui .message').transition('slide up');
  }

  clear() {
    this.name = '';
    this.contact = '';
    this.duplicate.name = '';
    this.duplicate.regdate = '';
  }

  filterCustomers() {
    this.customer_list = [];
    if (this.filter.length > 0) {
      let filtered = [];
      for (let i = 0; i < this.filter_list.length; i++) {
        if (this.filter_list[i].name.includes(this.filter) || this.filter_list[i].contact.includes(this.filter)) {
          filtered.push(this.filter_list[i]);
        }
      }
      this.customer_list = filtered;
      return
    }
    this.customer_list = this.filter_list;
  }

  selectCustomer(cust: any) {
    this.name = cust.name;
    this.contact = cust.contact;
    this.duplicate.regdate = cust.regdate;
    this.duplicate.name = cust.name;
  }
}
