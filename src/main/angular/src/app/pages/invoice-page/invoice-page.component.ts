import {Component, OnInit} from '@angular/core';
import {ItemService} from "../../service/item.service";
import {CustomerServiceService} from "../../service/customer-service.service";
import {InvoiceService} from "../../service/invoice.service";
import {NotificationService} from "../../service/notification.service";

declare var $: any;

@Component({
  selector: 'app-invoice-page',
  templateUrl: './invoice-page.component.html',
  styleUrls: ['./invoice-page.component.css']
})
export class InvoicePageComponent implements OnInit {

  item_list: any = [];
  item_list_filter: any = [];
  cart: any = [];
  total: any = '0.00';
  filter = '';
  cu_contact = '';
  cu_name = '';
  returning_customer = false;
  pay_by = 'cash';

what_name = '';
what_no = '';
  constructor(private itemService: ItemService, private CustomerService: CustomerServiceService,
              private invoiceService: InvoiceService, protected _notificationSvc: NotificationService) {
  }

  ngOnInit(): void {
    this.getItems();
  }

  private getItems() {
    this.itemService.item_list().subscribe(res => {
      this.item_list = res;
      this.item_list_filter = res;
    });
  }

  filterItems() {
    this.item_list = [];
    if (this.filter.length > 0) {
      let filtered = [];
      for (let i = 0; i < this.item_list_filter.length; i++) {
        if (this.item_list_filter[i].name.includes(this.filter)) {
          filtered.push(this.item_list_filter[i]);
        }
      }
      this.item_list = filtered;
      return
    }
    this.item_list = this.item_list_filter;
  }

  checkCustomer() {
    this.CustomerService.check_customer(this.cu_contact).subscribe(res => {
      if (res === null) {
        this.cu_name = '';
        this.returning_customer = false;
        this._notificationSvc.success('New Customer!', 'Please enter customer name and this customer will be save automatically.', 6000);
      } else {
        this.cu_name = res.name;
        this.returning_customer = true;
        this._notificationSvc.success('Returning Customer', '', 5000);
      }
    })
  }

  addToCart(item: any) {
    for (let i = 0; i < this.cart.length; i++) {
      if (this.cart[i].id === item.id) {
        this.cart[i].qty = (parseFloat(this.cart[i].qty) + 1);
        this.itemsubTotal(this.cart[i]);
        return
      }
    }
    let new_item = {
      id: item.id,
      name: item.name,
      expire: item.expire,
      cost: item.cost,
      price: item.price,
      qty: 1,
      max: item.qty,
      sub_total: item.price
    }
    this.cart.push(new_item);
    this.itemsubTotal(this.cart[this.cart.indexOf(new_item)]);
  }

  removeQty(item: any) {
    const index: number = this.cart.indexOf(item);
    if (this.cart[index].qty === 1) {
      if (index !== -1) {
        this.cart.splice(index, 1);
      }
      return;
    }
    this.cart[index].qty = this.cart[index].qty - 1;
    this.itemsubTotal(item);
  }

  addQty(item: any) {
    const index: number = this.cart.indexOf(item);
    if (parseFloat(this.cart[index].max) === parseFloat(this.cart[index].qty)) {
      alert('Out Of Stock!');
      return;
    }
    this.cart[index].qty = this.cart[index].qty + 1;
    this.itemsubTotal(item);
    return
  }

  itemsubTotal(item: any) {
    item.sub_total = (item.price * item.qty);
    this.totalAmount();
  }

  private totalAmount() {
    let tot = 0.00;
    for (let i = 0; i < this.cart.length; i++) {
      tot = (tot + parseFloat(this.cart[i].sub_total));
    }
    this.total = tot.toFixed(2);
  }

  removeItem(item: any) {
    const index: number = this.cart.indexOf(item);
    if (index !== -1) {
      this.cart.splice(index, 1);
      this.totalAmount();
      return
    }
  }

  checkQuantity(item: any) {
    if (parseFloat(item.qty) > parseFloat(item.max)) {
      alert('Out Of Stock!\nYou can add maximum ' + item.max + ' ' + item.name + ' for this invoice.')
      item.qty = item.max;
    } else if (parseFloat(item.qty) < 1) {
      const index: number = this.cart.indexOf(item);
      if (index !== -1) {
        this.cart.splice(index, 1);
        return
      }
    }
    this.itemsubTotal(item);
  }

  togglePayment(type: any) {
    this.pay_by = type;
  }

  createInvoice() {
    if (this.cu_contact.length < 9) {
      this._notificationSvc.info('Contact number not valid!', 'Please correct customer contact number.', 5000);
      return;
    } else if (this.cart.length < 1) {
      this._notificationSvc.info('Cart is empty!', 'Please add items first.', 3000);
      return;
    }
    let items: any = [];
    for (let i = 0; i < this.cart.length; i++) {
      let xx = {
        item: this.cart[i].id,
        item_name: this.cart[i].name,
        qty: this.cart[i].qty,
        price: this.cart[i].price,
        sub_total: parseFloat(this.cart[i].sub_total)
      }
      items.push(xx);
    }
    this.invoiceService.create_invoice(this.cu_contact, this.cu_name, this.pay_by,
      this.cart.length, this.total, items).subscribe(res => {
      this.readyForNext();
    }, error => {
      this._notificationSvc.error('Something went wrong!', 'Unexpected error occurred!\nPlease try again later.', 3000);
    });
  }

  private readyForNext() {
    this.what_name = this.cu_name;
    this.what_no = this.cu_contact;
    this.cart = [];
    this.getItems();
    this.total = '0.00';
    this.filter = '';
    this.cu_contact = '';
    this.cu_name = '';
    this.returning_customer = false;
    this.pay_by = 'cash';
    $('.ui.basic.modal').modal('show');
  }

  // @ts-ignore
  sendWhatsapp() {
    let token = 'eacudo2ibo3b46ln';
    let instanceId = '302798';
    let url = `https://api.chat-api.com/instance${instanceId}/message?token=${token}`;
    let data = {
      phone: this.what_no, // Receivers phone
      body: 'Hello, '+this.what_name+' \nPlease download your invoice by clicking this link.\n' +
        'http://5.231.208.76:8080/api/api/invoice/Invoice.pdf', // Message
    };
// Send a request
    $.ajax(url, {
      data: JSON.stringify(data),
      contentType: 'application/json',
      type: 'POST'
    });
    this._notificationSvc.success('Invoice send!', 'Invoice has been send to ' + this.what_no + '' +
      'via whatsapp.', 3000);
  }
}

