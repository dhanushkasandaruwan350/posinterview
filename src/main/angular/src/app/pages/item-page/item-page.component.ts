import {Component, OnInit} from '@angular/core';
import {ItemService} from "../../service/item.service";
import {NotificationService} from "../../service/notification.service";

@Component({
  selector: 'app-item-page',
  templateUrl: './item-page.component.html',
  styleUrls: ['./item-page.component.css']
})
export class ItemPageComponent implements OnInit {
  id: any;
  name: any;
  qty: any;
  cost: any;
  price: any;
  expire: any;

  showDuplicate = false;
  itemList: any = [];
  filterList: any = [];
  key = '';

  constructor(private itemService: ItemService, protected _notificationSvc: NotificationService) {
  }

  ngOnInit(): void {
    this.getItems();
  }

  private getItems() {
    this.itemService.item_list().subscribe(res => {
      this.itemList = res;
      this.filterList = res;
    });
  }

  checkID() {
    if (this.id.length < 1) {
      this.showDuplicate = false;
      return;
    }
    this.itemService.check_item(this.id).subscribe(res => {
      if (res !== null) {
        this.showDuplicate = true;
      } else {
        this.showDuplicate = false;
      }
    }, error => {
      console.log('www');
    });
  }

  filter() {
    this.itemList = [];
    if (this.key.length > 0) {
      let filtered = [];
      for (let i = 0; i < this.filterList.length; i++) {
        if (this.filterList[i].name.includes(this.key)) {
          filtered.push(this.filterList[i]);
        }
      }
      this.itemList = filtered;
      return
    }
    this.itemList = this.filterList;
  }

  saveItem() {
    if (!this.isFormFilled()) {
      return
    }
    this.itemService.create_item(this.id, this.name, this.qty, this.cost, this.price, this.expire).subscribe(res => {
      this.clear();
      this._notificationSvc.success('Successfully Created!', 'Item will be available for invoices shortly.');
      this.getItems();
    }, error => {
      this._notificationSvc.error('Ohh!', 'Something went wrong!\nPlease try again');
    })
  }

  private isFormFilled() {
    if (this.id === undefined || this.id.length < 1) {
      this._notificationSvc.warning('All fields are required!', "Enter an id for this item.");
      return false;
    } else if (this.name === undefined || this.name.length < 1) {
      this._notificationSvc.warning('All fields are required!', "You missed item name.");
      return false;
    } else if (this.qty === undefined || this.qty.length < 1) {
      this._notificationSvc.warning('All fields are required!', "Please specify quantity.");
      return false;
    } else if (this.cost === undefined || this.cost.length < 1) {
      this._notificationSvc.warning('All fields are required!', "Purchase cost is required!");
      return false;
    } else if (this.price === undefined || this.price.length < 1) {
      this._notificationSvc.warning('All fields are required!', "Please specify selling price.");
      return false;
    } else if (this.expire === undefined || this.expire.length < 1) {
      this._notificationSvc.warning('All fields are required!', "Expire date can not be empty.");
      return false;
    }
    return true;
  }

  clear() {
    this.id = '';
    this.name = '';
    this.qty = '';
    this.cost = '';
    this.price = '';
    this.expire = '';
    this.showDuplicate = false;
  }

  selectItem(item: any) {
    this.id = item.id;
    this.name = item.name;
    this.qty = item.qty;
    this.cost = item.cost;
    this.price = item.price;
    this.expire = item.expire;
    this.showDuplicate = true;
  }
}
