import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthenticationService} from "../../service/authentication-service.service";
import {NotificationService} from "../../service/notification.service";

declare var $: any;

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  username = ''
  password = ''

  constructor(private router: Router, private loginservice: AuthenticationService, protected _notificationSvc: NotificationService) {
  }

  ngOnInit(): void {

  }

  checkLogin() {
    (this.loginservice.authenticate(this.username, this.password).subscribe(
        data => {
          this.router.navigate(['/index/dash'])
        },
        error => {
          this._notificationSvc.warning('Try Again!', "Can not find account associate with " + this.username + "" +
            "\n Please double check and try again.", 3000);
        }
      )
    );

  }
}
