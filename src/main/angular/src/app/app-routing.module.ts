import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginPageComponent} from "./pages/login-page/login-page.component";
import {IndexPageComponent} from "./pages/index-page/index-page.component";
import {DashPageComponent} from "./pages/dash-page/dash-page.component";
import {CustomersPageComponent} from "./pages/customers-page/customers-page.component";
import {ItemPageComponent} from "./pages/item-page/item-page.component";
import {InvoicePageComponent} from "./pages/invoice-page/invoice-page.component";
import {GenerateInvoiceComponent} from "./pages/generate-invoice/generate-invoice.component";

const routes: Routes = [
  {
    path: 'sign-in', component: LoginPageComponent
  },
  {
    path: 'index', component: IndexPageComponent, children: [
      {
        path: '', component: DashPageComponent
      },
      {
        path: 'dash', component: DashPageComponent
      },
      {
        path: 'customers', component: CustomersPageComponent
      },
      {
        path: 'items', component: ItemPageComponent
      },
      {
        path: 'invoice', component: InvoicePageComponent
      },
      {
        path: 'invoice_generator', component: GenerateInvoiceComponent
      },
    ]
  },
  {
    path: '**', redirectTo: 'sign-in'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
