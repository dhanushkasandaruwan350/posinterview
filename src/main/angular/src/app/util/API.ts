export class API {
  public static HTTP_PREFIX = 'http://5.231.208.76:8080/api/api/';

  public static ENDPOINTS = {
    sign_in: {
      url: API.HTTP_PREFIX + 'auth/authenticate',
      type: 'POST'
    },
    create_customer: {
      url: API.HTTP_PREFIX + 'customer/create',
      type: 'POST'
    },
    check_customer: {
      url: API.HTTP_PREFIX + 'customer/check',
      type: 'POST'
    },
    customer_list: {
      url: API.HTTP_PREFIX + 'customer/list',
      type: 'GET'
    },
    create_item: {
      url: API.HTTP_PREFIX + 'item/create',
      type: 'POST'
    },
    check_item: {
      url: API.HTTP_PREFIX + 'item/check',
      type: 'POST'
    },
    list_item: {
      url: API.HTTP_PREFIX + 'item/list',
      type: 'GET'
    },
    create_invoice: {
      url: API.HTTP_PREFIX + 'invoice/create',
      type: 'POST'
    },
  }
}
