import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {API} from "../util/API";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor(private httpClient: HttpClient) {
  }

  // @ts-ignore
  create_item(id, name, qty, cost, price, expire) {
    return this.httpClient
      .post<any>(API.ENDPOINTS.create_item.url, {id, name, qty, cost, price, expire})
      .pipe(map(itemData => {
          return itemData;
        })
      );
  }

  // @ts-ignore
  check_item(id) {
    return this.httpClient
      .post<any>(API.ENDPOINTS.check_item.url, id)
      .pipe(map(itemData => {
          return itemData;
        })
      );
  }

  item_list() {
    return this.httpClient.get<any>(API.ENDPOINTS.list_item.url)
      .pipe(map(itemData => {
        return itemData;
      }));
  }
}
