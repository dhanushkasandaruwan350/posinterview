import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {API} from "../util/API";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class CustomerServiceService {

  constructor(private httpClient: HttpClient) {
  }

  // @ts-ignore
  create_customer(name, contact) {
    return this.httpClient
      .post<any>(API.ENDPOINTS.create_customer.url, {name, contact})
      .pipe(map(userData => {
          return userData;
        })
      );
  }

  // @ts-ignore
  check_customer(contact) {
    return this.httpClient
      .post<any>(API.ENDPOINTS.check_customer.url, contact)
      .pipe(map(userData => {
          return userData;
        })
      );
  }

  customer_list() {
    return this.httpClient.get<any>(API.ENDPOINTS.customer_list.url)
      .pipe(map(userData => {
        return userData;
      }));
  }
}
