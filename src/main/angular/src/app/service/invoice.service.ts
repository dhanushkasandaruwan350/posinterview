import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {API} from "../util/API";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  constructor(private httpClient: HttpClient) {
  }

  // @ts-ignore
  create_invoice(customer_contact, customer_name, pay_by, item_count, total, list) {
    return this.httpClient
      .post<any>(API.ENDPOINTS.create_invoice.url, {customer_contact, customer_name, pay_by, item_count, total, list})
      .pipe(map(invoiceData => {
          return invoiceData;
        })
      );
  }

}
