import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginPageComponent} from './pages/login-page/login-page.component';
import {IndexPageComponent} from './pages/index-page/index-page.component';
import {DashPageComponent} from './pages/dash-page/dash-page.component';
import {AdminNavComponent} from './navigations/admin-nav/admin-nav.component';
import {CustomersPageComponent} from './pages/customers-page/customers-page.component';
import {ItemPageComponent} from './pages/item-page/item-page.component';
import {CustomDropDownComponent} from './components/custom-drop-down/custom-drop-down.component';
import {InvoicePageComponent} from './pages/invoice-page/invoice-page.component';
import {CustomCardComponent} from './components/custom-card/custom-card.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {BasicAuthHtppInterceptorService} from "./service/basic-auth-htpp-interceptor-service.service";
import {FormsModule} from "@angular/forms";
import {GenerateInvoiceComponent} from './pages/generate-invoice/generate-invoice.component';
import {NotificationComponent} from './components/notification/notification.component';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    IndexPageComponent,
    DashPageComponent,
    AdminNavComponent,
    CustomersPageComponent,
    ItemPageComponent,
    CustomDropDownComponent,
    InvoicePageComponent,
    CustomCardComponent,
    GenerateInvoiceComponent,
    NotificationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: BasicAuthHtppInterceptorService,
    multi: true
  }, {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
